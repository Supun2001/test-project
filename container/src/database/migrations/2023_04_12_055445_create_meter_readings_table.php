<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('meter_readings', function (Blueprint $table) {
            $table->id();
            $table->date('last_reading_date');
            $table->date('previous_reading_date');
            $table->integer('last_meter_reading');
            $table->integer('previous_meter_reading');

            $table->unsignedBigInteger('customer_customer_account_number');
            $table->foreign('customer_customer_account_number')->references('customer_account_number')->on('customers');

            // $table->foreignId('customer_customer_account_number')
            // ->constrained()
            // ->onUpdate('cascade')
            // ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('meter_readings');
    }
};
